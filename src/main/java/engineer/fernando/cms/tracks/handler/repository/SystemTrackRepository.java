package engineer.fernando.cms.tracks.handler.repository;

import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.Optional;

public interface SystemTrackRepository extends JpaRepository<SystemTrack,Long> {
    Optional<SystemTrack> findBySystemTrackId(String systemtrackId);

    @Query(value = "SELECT (MAX(id)+1) FROM SystemTrack")
    public BigDecimal getNextId();
}
