package engineer.fernando.cms.tracks.handler.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import engineer.fernando.cms.tracks.handler.model.dao.AISTrackInfoDAO;
import engineer.fernando.cms.tracks.handler.service.AISService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component()
public class AISWebSocketHandler implements WebSocketHandler  {

	@Autowired
	private AISService aisService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AISWebSocketHandler.class);
	@Override
	public List<String> getSubProtocols() {
		return WebSocketHandler.super.getSubProtocols();
	}

	@Override
	public Mono<Void> handle(WebSocketSession webSocketSession) {
		Flux<WebSocketMessage> whatSending = aisService.produceAISTrackInfoFlux()
			.map(this::processRecordToJson)
			.map(webSocketSession::textMessage);
		return webSocketSession.send(whatSending)
			.and(webSocketSession.receive()
				.map(WebSocketMessage::getPayloadAsText).log()
			);
	}


	private String processRecordToJson(AISTrackInfoDAO aisTrackInfo) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(mapper.writeValueAsString(aisTrackInfo));
			((ObjectNode) jsonNode).put("type", "CMS_AISFLUX_MESSAGE" );
			return mapper.writeValueAsString(jsonNode);
		} catch (JsonProcessingException e) {
			if(LOGGER.isErrorEnabled()){
				LOGGER.error("Error while serializing to JSON",e);
			}
			return "Error while serializing to JSON";
		}
	}

}
