package engineer.fernando.cms.tracks.handler.model.dao;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;
import engineer.fernando.cms.tracks.handler.model.entity.TrackTacticalSituation;
import engineer.fernando.cms.tracks.handler.util.TacticalTools;

/**
 * TrackPerformanceDAO contains not only the built factors but also dynamic parameters that affects the whole performance
 *
 */
public class TrackPerformanceDAO extends TrackSpecification {
    private static final double FUEL_DENSITY_TON_PER_M_3 = 0.86;
    private static final double GALLON_PER_CUBIC_METER = 264.172;
    private static final double CUBIC_METER_PER_GALLON = 0.00378541;
    private  static final double NAVALSHIP_FUEL_CONSUMATION_PER_SPEED_FACTOR = 0.1;
    private static final double MISSILE_FUEL_CONSUMATION_PER_SPEED_FACTOR = 0.0001;

    private  static final double NAVALSHIP_FUEL_AUTONOMY_PER_DISTANCE = 1;
    private static final double MISSILE_FUEL_AUTONOMY_PER_DISTANCE = 10;

    @JsonProperty(access = Access.WRITE_ONLY)
    private final SystemTrack systemTrack;
    @JsonProperty(access = Access.WRITE_ONLY)
    private final TrackTacticalSituation lastTacticalUpdate;

    private final String trackId;

    private final double idealCruiseSpeed;
    private final double efficienceSpeedFactor;
    private final double fuelConsumationPerSeconds;
    private final double fuelWeight;
    private final double totalWeight;

    @JsonProperty(access = Access.WRITE_ONLY)
    private final double nextSpeed;
    @JsonProperty(access = Access.WRITE_ONLY)
    private final double nextCourse;
    @JsonProperty(access = Access.WRITE_ONLY)
    private final double nextFuelInGallons;

    public TrackPerformanceDAO(SystemTrack track, TrackTacticalSituation lastTacticalUpdate, int cycles) {
        super(track.getTrackSpecification());
        this.systemTrack = track;
        this.trackId = track.getSystemTrackId();
        this.lastTacticalUpdate = lastTacticalUpdate;
        this.nextSpeed = computeUpdatedSpeed(cycles);
        this.nextCourse = computeUpdatedCourse(cycles);
        this.fuelWeight = Math.max(0,lastTacticalUpdate.getFuelInGallons() * CUBIC_METER_PER_GALLON * FUEL_DENSITY_TON_PER_M_3);
        this.totalWeight = this.getGrossTonnage() + this.fuelWeight;
        this.idealCruiseSpeed = computeCruiseIdealSpeed();
        this.efficienceSpeedFactor = getEfficienceFactorOutOfIdealSpeed();
        this.fuelConsumationPerSeconds = computeFuelConsumationRate();
        this.nextFuelInGallons = computeUpdatedFuel();
    }


    public double getNextSpeed() {
        return nextSpeed;
    }

    public double getNextCourse() {
        return nextCourse;
    }
    public double getNextFuelInGallons() {
        return nextFuelInGallons;
    }

    public String getTrackId() {
        return trackId;
    }

    public double getIdealCruiseSpeed() {
        return idealCruiseSpeed;
    }

    public double getEfficienceSpeedFactor() {
        return efficienceSpeedFactor;
    }

    public double getFuelConsumationPerSeconds() {
        return fuelConsumationPerSeconds;
    }

    public double getFuelWeight() {
        return fuelWeight;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    /** Fuel Consumed is computed :
     * a) from the Fuel consumation per over weight factor
     * b) multiplied by inneficience factor (computed from how far is the current speed for the cruise speed.
     * c) cruise speed is determined according ship weight and type.
     * @return
     */
    private double computeFuelConsumationRate() {
        if(this.efficienceSpeedFactor > 0){
            return (getOverWeightFactor() * lastTacticalUpdate.getSpeedOverGround() * getFuelConsumationPerSpeedFactor() / this.efficienceSpeedFactor);
        }
        return 0;
    }

    private double computeUpdatedFuel() {
        return Math.max(0,lastTacticalUpdate.getFuelInGallons() - this.fuelConsumationPerSeconds);
    }

    private double getOverWeightFactor(){
        return this.totalWeight/this.getGrossTonnage();
    }

    private double getEfficienceFactorOutOfIdealSpeed(){
        return 1 -  Math.abs(lastTacticalUpdate.getSpeedOverGround() - this.idealCruiseSpeed)/(lastTacticalUpdate.getSpeedOverGround() + this.idealCruiseSpeed);
    }

    private double computeCruiseIdealSpeed(){
        final double referenceSpeed;
        switch (this.getTrackType()){
            case WEAPON:
                referenceSpeed = 5000 / getOverWeightFactor();
                break;
            case NAVALSHIP: default:
                referenceSpeed = 23 / getOverWeightFactor();
                break;
        }
        return referenceSpeed;
    }

    private double getFuelConsumationPerSpeedFactor(){
        switch (this.getTrackType()){
            case WEAPON:
                return MISSILE_FUEL_CONSUMATION_PER_SPEED_FACTOR / MISSILE_FUEL_AUTONOMY_PER_DISTANCE;
            case NAVALSHIP: default:
                return NAVALSHIP_FUEL_CONSUMATION_PER_SPEED_FACTOR / NAVALSHIP_FUEL_AUTONOMY_PER_DISTANCE;
        }
    }

    /**
     * Compute the next SpeedOverGround taking the acceleration factor into account
     * @return the Speed Over Ground for the next update cycle
     */
    private double computeUpdatedSpeed(int cycles) {
        if(this.lastTacticalUpdate.getSpeedOverGround() != this.systemTrack.getOrderedSpeedOverGround()){
            //IV - Compute future Speed Over Ground after 1 cycle of 1sec, considering acceleration factor
            double deltaSpeed = systemTrack.getOrderedSpeedOverGround() - lastTacticalUpdate.getSpeedOverGround();
            if(deltaSpeed >0 ){
                deltaSpeed = Math.min(cycles * this.getAcceleration(),deltaSpeed);
            }
            else{
                deltaSpeed = Math.max(-1 * cycles * this.getAcceleration(),deltaSpeed);
            }
            return lastTacticalUpdate.getSpeedOverGround() + deltaSpeed;

        }
        return this.lastTacticalUpdate.getSpeedOverGround();
    }

    /**
     * Compute the next CourseOverGround taking the angular acceleration factor into account
     * @return the Course Over Ground for the next update cycle
     */
    private double computeUpdatedCourse(int cycles){
        if(lastTacticalUpdate.getCourseOverGround() != systemTrack.getOrderedCourseOverGround()){
            //V - Compute future Course Over Ground after 1 cycle of 1sec, considering angular acceleration factor
            double deltaCourses =
                    TacticalTools.courseInDegreesToAzimuth(
                            this.systemTrack.getOrderedCourseOverGround()
                                    - lastTacticalUpdate.getCourseOverGround()
                    );
            if(deltaCourses > 0 ){
                deltaCourses = Math.min(cycles * this.getAngularAcceleration(),deltaCourses);
            }
            else{
                deltaCourses = Math.max(-1 * cycles * this.getAngularAcceleration(),deltaCourses);
            }
            double updatedCourse = TacticalTools.normalizeCourseInDegrees(
                    lastTacticalUpdate.getCourseOverGround() + deltaCourses
            );
            while(updatedCourse < 0 || updatedCourse > 360){
                updatedCourse = TacticalTools.normalizeCourseInDegrees(updatedCourse);
            }
            return updatedCourse;
        }
        return lastTacticalUpdate.getCourseOverGround();
    }

    /**
     * Compute Self Noise produced by track Speed taking into account a linear and a quadratic coefficient
     * @param speedInKnots the current Speed Over Ground in Knots
     * @return Self Noise in DB
     */
    public double computeSelfNoise(double speedInKnots) {
        return (this.getNoiseDueSpeedLinearCoefficient() * speedInKnots ) + (this.getNoiseDueSpeedQuadraticCoefficient() * Math.pow(speedInKnots,2));
    }

    @Override
    public String toString() {
        return "TrackPerformanceDAO{" +
                "idealCruiseSpeed=" + idealCruiseSpeed +
                ", efficienceSpeedFactor=" + efficienceSpeedFactor +
                ", fuelConsumationPerSeconds=" + fuelConsumationPerSeconds +
                ", fuelWeight=" + fuelWeight +
                ", totalWeight=" + totalWeight +
                '}';
    }
}
