package engineer.fernando.cms.tracks.handler.messages;

import lombok.Data;

@Data
public class TrackHandlerMessage {
    private long timestamp = System.currentTimeMillis();
    private String component = "TRACKHANDLER";
    private String topic;
    private String trackId;
    private Object message;

    public TrackHandlerMessage() {
    }

    public TrackHandlerMessage(String topic, String trackId, Object message) {
        this.topic = topic;
        this.trackId = trackId;
        this.message = message;
    }
}
