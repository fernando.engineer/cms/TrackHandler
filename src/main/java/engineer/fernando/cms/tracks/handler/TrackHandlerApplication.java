package engineer.fernando.cms.tracks.handler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


@SpringBootApplication
@EnableScheduling
@EnableJpaRepositories
public class TrackHandlerApplication implements AsyncConfigurer {
	public static void main(String[] args) {
		SpringApplication.run(TrackHandlerApplication.class, args);
	}

	@Override
	public Executor getAsyncExecutor() {
		return Executors.newScheduledThreadPool(1);
	}

}
