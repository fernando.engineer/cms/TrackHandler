package engineer.fernando.cms.tracks.handler.model;

import engineer.fernando.cms.domainmodel.avro.TrackType;
import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;


public class TrackSpecificationBuilder {

    private SystemTrack systemTrack;
    private TrackType trackType;
    private double beam;
    private double loa;
    private double draught;
    private double grossTonnage;
    private double netTonnage;
    private double maxSpeedOverGround;
    private double angularAcceleration;
    private double acceleration;
    private int weaponsCapacity;
    private double fuelCapacityInGallons;
    private double noiseDueSpeedLinearCoefficient;
    private double  noiseDueSpeedQuadraticCoefficient;
    private double sonarDetectionRange;
    private double sonarDetectionAzimuthRange;
    private double radarTransmittedPulsePower;
    private double radarDetectionRange;
    private double radarDetectionAzimuthRange;
    private double radarAntennaTurnSpeed;
    private double directivityDetectionGain;
    private double directivityDetectionAzimuth;

    public TrackSpecificationBuilder setSystemTrack(SystemTrack systemTrack) {
        this.systemTrack = systemTrack;
        return this;
    }

    public TrackSpecificationBuilder setTrackType(TrackType trackType) {
        this.trackType = trackType;
        return this;
    }

    public TrackSpecificationBuilder setBeam(double beam) {
        this.beam = beam;
        return this;
    }

    public TrackSpecificationBuilder setLoa(double loa) {
        this.loa = loa;
        return this;
    }

    public TrackSpecificationBuilder setDraught(double draught) {
        this.draught = draught;
        return this;
    }

    public TrackSpecificationBuilder setGrossTonnage(double grossTonnage) {
        this.grossTonnage = grossTonnage;
        return this;
    }

    public TrackSpecificationBuilder setNetTonnage(double netTonnage) {
        this.netTonnage = netTonnage;
        return this;
    }

    public TrackSpecificationBuilder setMaxSpeedOverGround(double maxSpeedOverGround) {
        this.maxSpeedOverGround = maxSpeedOverGround;
        return this;
    }

    public TrackSpecificationBuilder setAngularAcceleration(double angularAcceleration) {
        this.angularAcceleration = angularAcceleration;
        return this;
    }

    public TrackSpecificationBuilder setAcceleration(double acceleration) {
        this.acceleration = acceleration;
        return this;
    }

    public TrackSpecificationBuilder setWeaponsCapacity(int weaponsCapacity) {
        this.weaponsCapacity = weaponsCapacity;
        return this;
    }

    public TrackSpecificationBuilder setFuelCapacityInGallons(double fuelCapacityInGallons) {
        this.fuelCapacityInGallons = fuelCapacityInGallons;
        return this;
    }

    public TrackSpecificationBuilder setNoiseDueSpeedLinearCoefficient(double noiseDueSpeedLinearCoefficient) {
        this.noiseDueSpeedLinearCoefficient = noiseDueSpeedLinearCoefficient;
        return this;
    }

    public TrackSpecificationBuilder setNoiseDueSpeedQuadraticCoefficient(double noiseDueSpeedQuadraticCoefficient) {
        this.noiseDueSpeedQuadraticCoefficient = noiseDueSpeedQuadraticCoefficient;
        return this;
    }

    public TrackSpecificationBuilder setSonarDetectionRange(double sonarDetectionRange) {
        this.sonarDetectionRange = sonarDetectionRange;
        return this;
    }

    public TrackSpecificationBuilder setSonarDetectionAzimuthRange(double sonarDetectionAzimuthRange) {
        this.sonarDetectionAzimuthRange = sonarDetectionAzimuthRange;
        return this;
    }

    public TrackSpecificationBuilder setRadarTransmittedPulsePower(double radarTransmittedPulsePower) {
        this.radarTransmittedPulsePower = radarTransmittedPulsePower;
        return this;
    }

    public TrackSpecificationBuilder setRadarDetectionRange(double radarDetectionRange) {
        this.radarDetectionRange = radarDetectionRange;
        return this;
    }

    public TrackSpecificationBuilder setRadarDetectionAzimuthRange(double radarDetectionAzimuthRange) {
        this.radarDetectionAzimuthRange = radarDetectionAzimuthRange;
        return this;
    }

    public TrackSpecificationBuilder setRadarAntennaTurnSpeed(double radarAntennaTurnSpeed) {
        this.radarAntennaTurnSpeed = radarAntennaTurnSpeed;
        return this;
    }

    public TrackSpecificationBuilder setDirectivityDetectionGain(double directivityDetectionGain) {
        this.directivityDetectionGain = directivityDetectionGain;
        return this;
    }

    public TrackSpecificationBuilder setDirectivityDetectionAzimuth(double directivityDetectionAzimuth) {
        this.directivityDetectionAzimuth = directivityDetectionAzimuth;
        return this;
    }

    public TrackSpecification build(){
        TrackSpecification specification = new TrackSpecification();
        specification.setSystemTrack(this.systemTrack);
        specification.setTrackType(this.trackType);
        specification.setBeam(this.beam);
        specification.setLoa(this.loa);
        specification.setDraught(this.draught);
        specification.setGrossTonnage(this.grossTonnage);
        specification.setNetTonnage(this.netTonnage);
        specification.setMaxSpeedOverGround(this.maxSpeedOverGround);
        specification.setAngularAcceleration(this.angularAcceleration);
        specification.setAcceleration(this.acceleration);
        specification.setWeaponsCapacity(this.weaponsCapacity);
        specification.setFuelCapacityInGallons(this.fuelCapacityInGallons);
        specification.setNoiseDueSpeedLinearCoefficient(this.noiseDueSpeedLinearCoefficient);
        specification.setNoiseDueSpeedQuadraticCoefficient(this.noiseDueSpeedQuadraticCoefficient);
        specification.setSonarDetectionRange(this.sonarDetectionRange);
        specification.setSonarDetectionAzimuthRange(this.sonarDetectionAzimuthRange);
        specification.setRadarTransmittedPulsePower(this.radarTransmittedPulsePower);
        specification.setRadarDetectionRange(this.radarDetectionRange);
        specification.setRadarDetectionAzimuthRange(this.radarDetectionAzimuthRange);
        specification.setRadarAntennaTurnSpeed(this.radarAntennaTurnSpeed);
        specification.setDirectivityDetectionGain(this.directivityDetectionGain);
        specification.setDirectivityDetectionAzimuth(this.directivityDetectionAzimuth);
        return specification;
    }
}
