package engineer.fernando.cms.tracks.handler.consumer;

import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.cms.domainmodel.avro.Track;
import engineer.fernando.cms.tracks.handler.messages.TrackCmd;
import engineer.fernando.cms.tracks.handler.service.AISMessageDecoderService;
import engineer.fernando.cms.tracks.handler.service.AISService;
import engineer.fernando.cms.tracks.handler.service.KafkaService;
import engineer.fernando.cms.tracks.handler.service.SystemTrackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SystemTrackConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemTrackConsumer.class);


    @Autowired
    private SystemTrackService systemTrackService;

    @Autowired
    private AISMessageDecoderService aisMessageDecoderService;

    @Autowired
    private AISService aisTrackService;

    @Autowired
    private KafkaService kafkaService;

    @RabbitListener(queues = "${spring.rabbitmq.queue.systemtrack}")
    public void listenTrackCreation(@Payload Track track){
        systemTrackService.consumeSystemTrack(track);
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue.systemtrack.order}")
    public void listenTrackOrder(@Payload TrackCmd trackCmd){
        systemTrackService.sendCommand(trackCmd.getTrackId(), trackCmd.getCommand(), (float) trackCmd.getValue());
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue.nmea}")
    public void listenNMEAMessages(@Payload List<String> nmeaMessages){
        kafkaService.sendAlertMessage(String.valueOf(nmeaMessages.size()),"NMEA Messages received from RabbitMQ:");
        try{
            List<AISMessage> decodedMessages = aisMessageDecoderService.decode(nmeaMessages);
            if(LOGGER.isInfoEnabled()){
                LOGGER.info(String.format("Received %d Decoded Messages.",decodedMessages.size()));
            }
            kafkaService.sendAlertMessage(String.valueOf(decodedMessages.size()),"AIS Messages recognized from AISDecoder:");
            decodedMessages.forEach( aisMessage -> aisTrackService.processAISMessage(aisMessage));
        }
        catch (Exception e){
            if(LOGGER.isErrorEnabled()){
                LOGGER.error(String.format("NMEA batch of messages not recognized, deleted from Rabbit queue. Batch: %s", nmeaMessages.toString()));
            }
        }

    }

}
