package engineer.fernando.cms.tracks.handler.model.entity;

import engineer.fernando.ais.decoder.messages.AISMessage;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.lang.reflect.InvocationTargetException;

@Entity
@Table(name = "aistracks")
@AttributeOverride(name = "mmsi", column = @Column(name = "mmsi", unique = true))
public class AISTrack extends AISStructure {

    @Column(name = "updateTime")
    private long updateTime;


    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public static AISTrack aisTrackfactory(AISMessage aisMessage)
        throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
        AISTrack aisTrack = new AISTrack();
        aisTrack.setMMSI(aisMessage.getSourceMmsi().getMMSI());
        aisTrack.setCreationTime(System.currentTimeMillis());
        aisTrack.setUpdateTime(System.currentTimeMillis());
        //Perform dynamic injection on aisTrackMessage obj matching its setters to respective aisMessage getters
        return (AISTrack) AISStructure.dynamicInjector(aisTrack, aisMessage);
    }

    public AISTrack updateAISTrack(AISMessage aisMessage)
        throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
        this.setUpdateTime(System.currentTimeMillis());
        //Perform dynamic injection on aisTrackMessage obj matching its setters to respective aisMessage getters
        return (AISTrack) AISStructure.dynamicInjector(this, aisMessage);
    }


    @Override
    public String toString() {
        return String.format("AISTrack [updateTime=%s, creationTime=%s, id=%s, mmsi=%s, messageType=%s]",
            this.getUpdateTime(),
            this.getCreationTime(),
            this.getId(),
            this.getMMSI(),
            this.getMessageType().getValue()
        );
    }
}
