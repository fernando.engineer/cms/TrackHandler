package engineer.fernando.cms.tracks.handler.repository;

import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TrackSpecificationRepository extends JpaRepository<TrackSpecification,Long> {

    Optional<TrackSpecification> findBySystemTrack(long systemtrackId);
}
