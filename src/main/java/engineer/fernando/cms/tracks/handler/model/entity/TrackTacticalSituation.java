package engineer.fernando.cms.tracks.handler.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "systemtracks_tactics")
public class TrackTacticalSituation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="systemTrackId", referencedColumnName="systemTrackId", foreignKey=@ForeignKey(name = "FK_systemTrack_tactics"))
    @JsonBackReference
    private SystemTrack systemTrack;

    @Column(name = "creationTime")
    private long creationTime;
    @Column(name = "latitude")
    private double latitude;
    @Column(name = "longitude")
    private double longitude;
    @Column(name = "speedOverGround")
    private double speedOverGround;
    @Column(name = "courseOverGround")
    private double courseOverGround;
    @Column(name = "distanceTravelled")
    private double distanceTravelled;
    @Column(name = "fuelInGallons")
    private double fuelInGallons;
    @Column(name = "dBNoise")
    private double dBNoise;

    public SystemTrack getSystemTrack() {return systemTrack;}

    public void setSystemTrack(SystemTrack systemTrack) {
        this.systemTrack = systemTrack;
        systemTrack.addTacticalUpdate(this);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {this.latitude = latitude;}

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSpeedOverGround() {return speedOverGround;}

    public void setSpeedOverGround(double speedOverGround) {
        this.speedOverGround = Math.round(speedOverGround * 100.0) / 100.0;
    }

    public double getCourseOverGround() {
        return courseOverGround;
    }

    public void setCourseOverGround(double courseOverGround) {
        this.courseOverGround = Math.round(courseOverGround * 100.0) / 100.0;
    }

    public double getDistanceTravelled() { return distanceTravelled; }

    public void setDistanceTravelled(double distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public double getFuelInGallons() {
        return fuelInGallons;
    }

    public void setFuelInGallons(double fuelInGallons) {
        this.fuelInGallons = Math.round(fuelInGallons * 100.0) / 100.0;
    }

    public double getdBNoise() {
        return dBNoise;
    }

    public void setdBNoise(double dBNoise) {
        this.dBNoise = Math.round(dBNoise * 100.0) / 100.0;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }


    @Override
    public String toString() {
        return "TrackTacticalSituation [id=" + id + ", systemTrack=" + systemTrack.getSystemTrackId()
                + ", Lat/Lng=" + latitude + "," + longitude + ",CoG="+ courseOverGround + ", SoG=" +speedOverGround
                + ", FuelInGallons=" + fuelInGallons + ", NoiseInDb=" +dBNoise + ", CreationTime=" +creationTime +" received.]";
    }
}
