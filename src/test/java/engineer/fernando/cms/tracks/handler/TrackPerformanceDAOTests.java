package engineer.fernando.cms.tracks.handler;

import engineer.fernando.cms.domainmodel.avro.Position;
import engineer.fernando.cms.domainmodel.avro.SensorTrackType;
import engineer.fernando.cms.tracks.handler.model.SystemTrackBuilder;
import engineer.fernando.cms.tracks.handler.model.TrackTacticalSituationBuilder;
import engineer.fernando.cms.tracks.handler.model.dao.TrackPerformanceDAO;
import engineer.fernando.cms.tracks.handler.model.entity.SystemTrack;
import engineer.fernando.cms.tracks.handler.model.entity.TrackSpecification;
import engineer.fernando.cms.tracks.handler.model.entity.TrackTacticalSituation;
import engineer.fernando.cms.tracks.handler.util.TrackSpecificationBundle;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static junit.framework.TestCase.assertEquals;

class TrackPerformanceDAOTests {

    @Test
    void testComputePerformance() {
        // Build specification from Ship template.
        TrackSpecification shipSpecification =
                TrackSpecificationBundle.buildTrackSpecification(SensorTrackType.OWNSHIP);

        SystemTrack track = new SystemTrackBuilder()
                .setPlayerId(31415).setSensorTrackType(SensorTrackType.OWNSHIP).setName("Antares")
                .setWeaponsAvailable(5).setOrderedSpeedOverGround(15.3).setOrderedCourseOverGround(314)
                .setTrackTechSpec(shipSpecification).setHit(false)
                .setSystemTrackId(BigDecimal.valueOf(1)).build();

        // Adding first tactical update for track
        Position position = new Position(0.98, 2.3, 0.0);
        TrackTacticalSituation tacticalUpdate = new TrackTacticalSituationBuilder().setSystemTrack(track)
                .setLatitude(position.getLatitude()).setLongitude(position.getLongitude())
                .setCourseOverGround(123.4).setSpeedOverGround(31.4).setdBNoise(20.5).setFuelInGallons(45.8).build();

        TrackPerformanceDAO trackPerformanceDAO = new TrackPerformanceDAO(track, tacticalUpdate, 1);


        double idealCruiseSpeed = trackPerformanceDAO.getIdealCruiseSpeed();
        assertEquals(22.9996, idealCruiseSpeed, 0.0001);
        double efficienceSpeedFactor = trackPerformanceDAO.getEfficienceSpeedFactor();
        assertEquals(0.845, efficienceSpeedFactor, 0.001);
        double fuelConsumationPerSeconds = trackPerformanceDAO.getFuelConsumationPerSeconds();
        assertEquals(3.71, fuelConsumationPerSeconds, 0.01);
        double fuelWeight = trackPerformanceDAO.getFuelWeight();
        assertEquals(0.15, fuelWeight, 0.01);
        double totalWeight = trackPerformanceDAO.getTotalWeight();
        assertEquals(10000.15, totalWeight, 0.01);
        assertEquals(122.60, trackPerformanceDAO.getNextCourse(), 0.1);
        assertEquals(29.9, trackPerformanceDAO.getNextSpeed(), 0.1);
        assertEquals(42.08, trackPerformanceDAO.getNextFuelInGallons(), 0.01);

    }
}